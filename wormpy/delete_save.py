import os
import sys
import shutil


def delete_save(gamesave_path):
    try:
        shutil.move(gamesave_path, gamesave_path + "-old")
        print("Saved game deleted.")
        print(f"A backup was created at `{gamesave_path}-old`. To restore it, run:")
        print(f"mv {gamesave_path}-old {gamesave_path}")
    except FileNotFoundError:
        print("No saved game found.")
    except Exception as e:
        print("An error occurred.")
        print(str(type(e)).split("'")[1] + ": " + str(e))
        cont = input(
            "Do you want to try to non-recoverably delete the saved game? [Y/n] "
        )
        if cont[0] in "Yy":
            try:
                os.unlink(gamesave_path)
                print("Saved game deleted.")
            except Exception as e:
                print("An error occurred.")
                print(str(type(e)).split("'")[1] + ": " + str(e))
            sys.exit(1)
        else:
            sys.exit(1)
    sys.exit(0)
